const basePath = '/helados-in-dh-group'

module.exports = {
  pathPrefix: basePath,
  siteMetadata: {
    title: `Helados team's Digital Publication`,
    description: `A website for Mith301.`,
    author: `María Agustina Ryckeboer and Sidra Nadeem`
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    `gatsby-theme-ceteicean`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/tei`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/pages`,
        name: `html`,
      },
    },
  ],
}
